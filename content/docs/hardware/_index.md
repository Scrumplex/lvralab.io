---
weight: 50
title: Hardware
---

# Hardware

A non-comprehensive table of various VR/XR devices and the drivers that support them.

| Device               | [SteamVR](/docs/steamvr/)             | [Monado](/docs/fossvr/monado/) | [WiVRn](/docs/fossvr/wivrn/) |
|----------------------|:-------------------------------------:|:------------------------------:|:----------------------------:|
| **PC VR**            |                                       |                                |                              |
| Valve Index          | ✅                                    | ✅                             | 🚧 (WiVRn PC-PC stream)      |
| HTC Vive             | ✅                                    | ✅                             | 🚧 (WiVRn PC-PC stream)      |
| HTC Vive Pro         | ✅                                    | ✅                             | 🚧 (WiVRn PC-PC stream)      |
| HTC Vive Pro Eye     | ✅ (No eyechip)                       | ✅ (No eyechip)                | 🚧 (WiVRn PC-PC stream)      |
| HTC Vive Pro 2       | ✅ (custom [driver and patches](https://github.com/CertainLach/VivePro2-Linux-Driver)) | ? (presumably with two kernel patches [1](https://github.com/CertainLach/VivePro2-Linux-Driver/blob/master/kernel-patches/0002-drm-edid-parse-DRM-VESA-dsc-bpp-target.patch) [2](https://github.com/CertainLach/VivePro2-Linux-Driver/blob/master/kernel-patches/0003-drm-amd-use-fixed-dsc-bits-per-pixel-from-edid.patch)) | --                           |
| Bigscreen Beyond     | ~ (Functional after monado has run)   | ✅ (with two kernel patches [1](https://github.com/CertainLach/VivePro2-Linux-Driver/blob/master/kernel-patches/0002-drm-edid-parse-DRM-VESA-dsc-bpp-target.patch) [2](https://github.com/CertainLach/VivePro2-Linux-Driver/blob/master/kernel-patches/0003-drm-amd-use-fixed-dsc-bits-per-pixel-from-edid.patch))                   | --                           |
| Somnium VR1          | ?                                     | ?                              | ?                            |
| VRgineers XTAL       | ?                                     | ?                              | ?                            |
| StarVR One           | ?                                     | ?                              | ?                            |
| Varjo VR-1           | ?                                     | ?                              | ?                            |
| Varjo VR-2           | ?                                     | ?                              | ?                            |
| Varjo VR-3           | ?                                     | ?                              | ?                            |
| Pimax 4K             | ❌ (Planned)                          | ❌ (Planned)                   | --                           |
| Pimax 5K Plus        | ❌ (Planned)                          | ❌ (Planned)                   | --                           |
| Pimax 8K             | ❌ (Planned)                          | ❌ (Planned)                   | --                           |
| Lenovo Explorer      | ✅ (Monado SteamVR plugin)            | ✅ (experimental 6dof controllers) | 🚧 (WiVRn PC-PC stream)      |
| Acer AH101           | ✅ (Monado SteamVR plugin)            | ✅ (experimental 6dof controllers) | 🚧 (WiVRn PC-PC stream)      |
| Dell Visor           | ✅ (Monado SteamVR plugin)            | ✅ (experimental 6dof controllers) | 🚧 (WiVRn PC-PC stream)      |
| HP WMR headset       | ✅ (Monado SteamVR plugin)            | ✅ (experimental 6dof controllers) | 🚧 (WiVRn PC-PC stream)      |
| Samsung Odyssey      | ✅ (Monado SteamVR plugin)            | ✅ (experimental 6dof controllers) | 🚧 (WiVRn PC-PC stream)      |
| Asus HC102           | ✅ (Monado SteamVR plugin)            | ✅ (experimental 6dof controllers) | 🚧 (WiVRn PC-PC stream)      |
| Samsung Odyssey+     | ✅ (Monado SteamVR plugin)            | ✅ (experimental 6dof controllers) | 🚧 (WiVRn PC-PC stream)      |
| HP Reverb            | ✅ (Monado SteamVR plugin)            | ✅ (experimental 6dof controllers) | 🚧 (WiVRn PC-PC stream)      |
| Acer OJO 500         | ✅ (Monado SteamVR plugin)            | ✅ (experimental 6dof controllers) | 🚧 (WiVRn PC-PC stream)      |
| HP Reverb G2         | ✅ (Monado SteamVR plugin)            | ✅ (experimental 6dof controllers) | 🚧 (WiVRn PC-PC stream)      |
| Oculus Rift CV1      | 🚧 (Monado SteamVR plugin)            | 🚧 (ancient openhmd based support) | 🚧 (WiVRn PC-PC stream)      |
| Oculus Rift S        | 🚧 (Monado SteamVR plugin)            | 🚧 (WIP)                       | 🚧 (WiVRn PC-PC stream)      |
| **Standalone**       |                                       |                                |                              |
| Quest                | ✅ (via [ALVR](/docs/steamvr/alvr/))  | --                             | ✅                           |
| Quest 2              | ✅ (via [ALVR](/docs/steamvr/alvr/))  | --                             | ✅                           |
| Quest Pro            | ✅ (via [ALVR](/docs/steamvr/alvr/))  | --                             | ✅                           |
| Quest 3              | ✅ (via [ALVR](/docs/steamvr/alvr/))  | --                             | ✅                           |
| Pico 4               | ✅ (via [ALVR](/docs/steamvr/alvr/))  | --                             | ✅                           |
| Pico Neo 3           | ✅ (via [ALVR](/docs/steamvr/alvr/))  | --                             | ✅                           |
| HTC Vive Focus 3     | ✅ (via [ALVR](/docs/steamvr/alvr/))  | --                             | ✅                           |
| HTC Vive XR Elite    | ✅ (via [ALVR](/docs/steamvr/alvr/))  | --                             | ✅                           |
| Lynx R1              | ✅ (via [ALVR](/docs/steamvr/alvr/))  | --                             | ✅                           |
| Apple Vision Pro     | ✅ (via [ALVR](/docs/steamvr/alvr/))  | --                             | ❌                           |
| **Trackers**         |                                       |                                |                              |
| Vive/Tundra trackers | ✅                                    | ✅                             | ❌                           |
| SlimeVR trackers     | ✅                                    | ✅ (Supported by OSC)          | 🚧 (Proposed)                |
| Project Babble       | ✅ (oscavmgr)                         | ✅ (oscavmgr)                  | ✅ (oscavmgr)                |
| Eyetrack VR          | 🚧 (WIP)                              | 🚧 (WIP)                       | 🚧 (WIP)                     |
| Mercury Handtrack    | 🚧 (Monado SteamVR plugin, win only)  | ✅ (survive driver only)       | ❌                           |
| Lucid VR Gloves      | ?                                     | ✅ (survive driver only)       | ❌                           |
| Standable FBT        | ❌                                    | ❌                             | ❌                           |

## Hardware note

WiVRn PC XR to WiVRn PC client streaming remains mainly for debug use.

Vive Pro microphones should be set to use 44.1khz sample rates as feeding in 48khz raises the pitch of the audio.

Valve index audio output should be set to 48khz or no audio will output.

Vive Pro Eye HMD functional, eyechip WIP.

Pimax initialization code WIP.

Tundra trackers preferred for full body, linear and angular velocity stop upon occlusion.

Eyetrack VR and Project Babble will both be implemented through [oscavmgr](https://github.com/galister/oscavmgr) to emit proper unified flexes over OSC.